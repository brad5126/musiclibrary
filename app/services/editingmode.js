import Service from '@ember/service';

export default Service.extend({

editingMode:false,


setEditingModeTrue: function(){
    this.set('editingMode',true)
    console.log("TURNING ON EDITING MODE")
},
setEditingModeFalse: function(){
    this.set('editingMode',false)
    console.log("TURNING OFF EDITING MODE")
}

});
