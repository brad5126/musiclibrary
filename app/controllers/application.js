import Controller from '@ember/controller';

export default Controller.extend({

    editingmode: Ember.inject.service(),

    actions:{

        setEditModeTrue(){
            this.editingmode.setEditingModeTrue();
        },
        setEditModeFalse(){
            this.editingmode.setEditingModeFalse();
        }

    }
});
