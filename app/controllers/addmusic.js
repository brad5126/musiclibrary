import Controller from '@ember/controller';

export default Controller.extend({
  store: Ember.inject.service(),

  actions: {



    save() {

      console.log("SAVING");

      console.log();

      let albumName = this.get('albumName');
      let artistName = this.get('artistName');
      let genre = this.get('genre');
      let spotifyLink = this.get('spotifyLink');
      let albumCover = this.get('albumCover');
      let yearOfRelease = this.get('yearOfRelease');

      let album = this.store.createRecord('album', {
        artistName: artistName,
        albumName: albumName,
        genre: genre,
        albumCover: albumCover,
        yearOfRelease: yearOfRelease,
        spotifyLink: spotifyLink
      });

      album.save();
      alert("ALBUM SAVED");

    },

    clear() {
      this.set('albumName', '');
      this.set('artistName', '');
      this.set('genre', '');
      this.set('spotifyLink', '');
      this.set('albumCover', '');
      this.set('yearOfRelease', '');

      alert("CLEARED")

    }


  }


});
