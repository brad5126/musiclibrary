export default function() {
  this.namespace = 'api';

  this.get('/albums', () => {

    return {
      albums: [
        {id: 1, artistName:'Drake',albumName: 'Scorpion',genre:'Hip hop',albumCover:'https://upload.wikimedia.org/wikipedia/en/thumb/9/90/Scorpion_by_Drake.jpg/220px-Scorpion_by_Drake.jpg',yearOfRelease:2018,spotifyLink:'https://open.spotify.com/album/1ATL5GLyefJaxhQzSPVrLX?si=gxskTrRBQwu8fZlQKuF8cw'},
        {id: 2, artistName:'Chris Minh Doky',albumName: 'Transparency',genre:'Jazz',albumCover:'http://www.yellow1.dk/wp-content/uploads/2018/02/chrisminhdoky_cover_1200px.jpg',yearOfRelease:2018,spotifyLink:'https://open.spotify.com/album/5BrQWw6zTW4rn155urmgg9?si=oHlYrn8QRDSkgjw0ve5fgg'},

      ]
    };
  });
  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `/api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    http://www.ember-cli-mirage.com/docs/v0.3.x/shorthands/
  */
}
